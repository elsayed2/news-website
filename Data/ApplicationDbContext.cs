﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;



namespace NewsWebsite.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            
            base.OnModelCreating(builder);

            // Default profile picture
            byte[] imageArray = File.ReadAllBytes(@"D:\WEB\Projects\NewsWebsite\profilePicture.png");

            // Make FirstName and LastName columns required and have max length 25 
            builder.Entity<ApplicationUser>(u =>
            {
                u.Property(user => user.ProfilePicture).HasDefaultValue(imageArray);

                u.Property(user => user.FirstName).IsRequired();
                u.Property(user => user.FirstName).HasMaxLength(25);

                u.Property(user => user.LastName).IsRequired();
                u.Property(user => user.LastName).HasMaxLength(25);
            });
        }
    }
}
