﻿using Microsoft.AspNetCore.Mvc;
using NewsWebsite.Models;
using System.Diagnostics;

namespace NewsWebsite.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly NewsContext _context;

        public HomeController(ILogger<HomeController> logger, NewsContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            // Select the categories and send them to the view
            var Categories = _context.Categories.ToList();
            return View(Categories);
        }
        public IActionResult ContactUs()
        {
            return View();
        }

        // Action for saving the content of the contact form in the database
        [HttpPost]
        public IActionResult SaveForm(ContactUs model)
        {
            if (ModelState.IsValid)
            {
                _context.ContactUs.Add(model);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("ContactUs", model);
        }

        // Select the reviews and send them to Reviews view
        public IActionResult Reviews()
        {
            return View(_context.ContactUs.ToList());
        }

        // Select news of a category and send them to News view
        public IActionResult News(int id)
        {
            var News = _context.News.Where(n => n.CategoryId == id)
                .OrderByDescending(n => n.Date)
                .ToList();

            //Select the category 
            var Category = _context.Categories.Find(id);
            ViewBag.Category = Category;

            return View(News);
        }

        // Select the team members and send them to TeamMembers view
        public IActionResult TeamMembers()
        {
            return View(_context.TeamMembers.ToList());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}