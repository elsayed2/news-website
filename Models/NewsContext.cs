﻿using Microsoft.EntityFrameworkCore;

namespace NewsWebsite.Models
{
    public class NewsContext:DbContext
    {
        //Tables
        public DbSet<News> News { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<TeamMember> TeamMembers { get; set; }
        public DbSet<ContactUs> ContactUs { get; set; }
        public NewsContext(DbContextOptions<NewsContext> options):base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Relation between Category and News Table
            modelBuilder.Entity<Category>()
                .HasMany(c => c.News)
                .WithOne(n => n.Category)
                .HasForeignKey(n => n.CategoryId);

            // Validation for ContactUs Model
            modelBuilder.Entity<ContactUs>(c =>
            {
                c.Property(cu => cu.Name).HasMaxLength(40);
                c.Property(cu => cu.Subject).HasMaxLength(80);
            });

            //NotMapped for File Property in News domain model
            modelBuilder.Entity<News>().Ignore(n => n.File);

            //NotMapped for File Property in TeamMember domain model
            modelBuilder.Entity<TeamMember>().Ignore(t => t.File);


        }
    }
}
