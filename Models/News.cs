﻿namespace NewsWebsite.Models
{
    public class News
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public IFormFile File { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Topic { get; set; }
        public Category Category { get; set; }
        public int CategoryId { get; set; }
    }
}
