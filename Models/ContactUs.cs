﻿using System.ComponentModel.DataAnnotations;

namespace NewsWebsite.Models
{
    public class ContactUs
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
