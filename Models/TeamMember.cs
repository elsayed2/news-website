﻿namespace NewsWebsite.Models
{
    public class TeamMember
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public IFormFile File { get; set; }
        public string Name { get; set; }
        public string JobTitle { get; set; }
        
    }
}
