﻿using Microsoft.AspNetCore.Identity;

namespace NewsWebsite.ViewModels
{
    public class UserRolesVm
    {
        public UserRolesVm()
        {
            Roles = new List<string>();
        }
        public IdentityUser User { get; set; }
        public List<string> Roles { get; set; }
    }
}
