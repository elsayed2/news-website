﻿namespace NewsWebsite.ViewModels
{
    public class RoleCheckBoxVm
    {
        public string RoleName { get; set; }
        public bool IsSelected { get; set; }
    }
}
