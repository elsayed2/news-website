﻿using System.ComponentModel.DataAnnotations;

namespace NewsWebsite.ViewModels
{
    public class EditVm
    {

        public string Id { get; set; }

        [Required]
        [MaxLength(25)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(25)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public List<RoleCheckBoxVm> Roles { get; set; }
    }
}
