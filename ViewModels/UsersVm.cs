﻿namespace NewsWebsite.ViewModels
{
    public class UsersVm
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public IEnumerable<string> UserRoles { get; set; }
    }
}
