# News website

## Description
This is a news website. Through this website, you can view news from every category. Additionally, you can write reviews, and these reviews will automatically appear on the Reviews page. You can also contact us, and the messages will be stored in the database. You can find our team members on the Team Members page. Regarding registration, you can also sign up with your Google account.
![website](/Website.jpg)

In the Admin Panel, you can create, read, update, and delete categories and news if your role is an admin or moderator. If your role is admin, you can also create, update, and delete users. New users who sign up will automatically receive the user role.
![AdminPanel](/AdminPanel.jpg)


