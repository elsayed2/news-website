﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NewsWebsite.Data;
using NewsWebsite.ViewModels;
using System.Net.Mail;

namespace NewsWebsite.Areas.AdminPanel.Controllers
{
    [Area("AdminPanel")]
    public class UsersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public UsersController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // Select all users and their roles
        public IActionResult Index()
        {
            var users = _userManager.Users.Select(user => new UsersVm
            {
                Id=user.Id,
                Name = string.Join(", ", new List<string> { user.FirstName, user.LastName }),
                Email = user.Email,
                UserRoles = _userManager.GetRolesAsync(user).Result
            }).ToList();

            return View(users);
        }

        // Select all roles and return viem model for the view
        public IActionResult Create()
        {
            var roles = _roleManager.Roles.Select(r => new RoleCheckBoxVm 
            {
                RoleName=r.Name
            }).ToList();

            var user = new CreateUserVm
            {
                Roles = roles
            };
            return View(user);
        }

        // Action to create a new user
        [HttpPost]
        public async Task<IActionResult> Create(CreateUserVm model)
        {
            //check for the model
            if(!ModelState.IsValid)
                return View(model);

            //check if the email is already registered
            if(await _userManager.FindByEmailAsync(model.Email)!=null)
            {
                ModelState.AddModelError("Email", "Email address is already registered");
                return View(model);
            }

            //Each user must have at least one role
            if (!model.Roles.Any(r=> r.IsSelected))
            {
                ModelState.AddModelError("Roles", "You must choose at least one role");
                return View(model);
            }

            // Add the user to the database
            var user = new ApplicationUser
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                UserName = new MailAddress(model.Email).User
            };
            var result =await _userManager.CreateAsync(user, model.Password);
            if(!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("Roles", error.Description);
                }
                return View(model);
            }

            // Add Roles to the user
            await _userManager.AddToRolesAsync(user, model.Roles.Select(r => r.RoleName));

            return RedirectToAction(nameof(Index));
        }

        // Select the user with the ID and send it to the view for editing
        public async Task<IActionResult> Edit(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
                return NotFound();

            var model = new EditVm
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Roles = _roleManager.Roles.Select(r => new RoleCheckBoxVm
                {
                    RoleName = r.Name,
                    IsSelected = _userManager.IsInRoleAsync(user,r.Name).Result
                }).ToList()
            };

            return View(model);
        }


        // An action to apply changes to the user
        [HttpPost]
        public async Task<IActionResult> Edit(EditVm model)
        {
            if (!ModelState.IsValid)
                return View(model);

            // Check for Id
            var user = await _userManager.FindByIdAsync(model.Id);
            if (user == null)
                return NotFound();

            //Check for Email
            var userWithSameEmail = await _userManager.FindByEmailAsync(model.Email);
            if(userWithSameEmail != null && userWithSameEmail.Id != model.Id)
            {
                ModelState.AddModelError("Email", "This Email is already assigned for another user");
                return View(model);
            }

            //Apply changes
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;


            // Add or remove roles from user
            var userRoles = await _userManager.GetRolesAsync(user);

            foreach (var role in model.Roles)
            {
                if (userRoles.Any(r => r == role.RoleName) && !role.IsSelected)
                    await _userManager.RemoveFromRoleAsync(user, role.RoleName);

                if(!userRoles.Any(r=> r==role.RoleName) && role.IsSelected)
                    await _userManager.AddToRoleAsync(user, role.RoleName);

            }

            await _userManager.UpdateAsync(user);

            return RedirectToAction(nameof(Index)); 
        }

        // Action for deleting user
        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
                return NotFound();
            var result=await _userManager.DeleteAsync(user);
                
            return RedirectToAction(nameof(Index));
        }
    }
}
