﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NewsWebsite.Data;
using NewsWebsite.Models;
using NewsWebsite.ViewModels;

namespace NewsWebsite.Areas.AdminPanel.Controllers
{
    [Area("AdminPanel")]
    [Authorize(Roles="Admin,Moderator")]
    public class TeamMembersController : Controller
    {
        private readonly NewsContext _context;
        private readonly IWebHostEnvironment _host;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public TeamMembersController(NewsContext context, IWebHostEnvironment host,UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _host = host;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // GET: TeamMembers
        public async Task<IActionResult> Index()
        {
            return View(await _context.TeamMembers.ToListAsync());
        }

        // GET: TeamMembers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TeamMembers == null)
            {
                return NotFound();
            }

            var teamMember = await _context.TeamMembers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (teamMember == null)
            {
                return NotFound();
            }

            return View(teamMember);
        }

        // GET: TeamMembers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TeamMembers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TeamMember teamMember)
        {
            ModelState.Remove("Image");
            if (ModelState.IsValid)
            {
                UploadPhoto(teamMember);
                _context.Add(teamMember);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(teamMember);
        }

        // GET: TeamMembers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TeamMembers == null)
            {
                return NotFound();
            }

            var teamMember = await _context.TeamMembers.FindAsync(id);
            if (teamMember == null)
            {
                return NotFound();
            }
            return View(teamMember);
        }

        // POST: TeamMembers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, TeamMember teamMember)
        {

            ModelState.Remove("Image");
            if (id != teamMember.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    UploadPhoto(teamMember);
                    _context.Update(teamMember);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TeamMemberExists(teamMember.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(teamMember);
        }

        // GET: TeamMembers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TeamMembers == null)
            {
                return NotFound();
            }

            var teamMember = await _context.TeamMembers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (teamMember == null)
            {
                return NotFound();
            }

            return View(teamMember);
        }

        // POST: TeamMembers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TeamMembers == null)
            {
                return Problem("Entity set 'NewsContext.TeamMembers'  is null.");
            }
            var teamMember = await _context.TeamMembers.FindAsync(id);
            if (teamMember != null)
            {
                _context.TeamMembers.Remove(teamMember);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // Select the user and their roles 
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> UserRoles()
        {
            var users = await _userManager.Users.ToListAsync();
            var roles = await _roleManager.Roles.ToListAsync();
            List<UserRolesVm> userRoles=new List<UserRolesVm>();
            foreach (var user in users)
            {
                var userroles=await _userManager.GetRolesAsync(user);
                userRoles.Add(new UserRolesVm { User = user, Roles = (List<string>)userroles}); 
            }
            ViewBag.Roles = roles;
            return View(userRoles); 

        }

        // Function to upload photos
        public void UploadPhoto(TeamMember member)
        {
            if (member.File != null)
            {
                string FolderPath = Path.Combine(_host.WebRootPath, "Images");
                string UniqueImageName = Guid.NewGuid() + ".jpg";
                string ImagePath = Path.Combine(FolderPath, UniqueImageName);
                using (var filestream = new FileStream(ImagePath, FileMode.Create))
                {
                    member.File.CopyTo(filestream);
                }
                member.Image = UniqueImageName;
            }
        }

        //Add or remove roles from user
        public async Task<IActionResult> AddRemoveRole(string userid,string role)
        {
            var user = await _userManager.FindByIdAsync(userid);
            var result = await _userManager.AddToRoleAsync(user,role);
            if(!result.Succeeded)
                await _userManager.RemoveFromRoleAsync(user, role);
            return RedirectToAction("UserRoles");
        }
        private bool TeamMemberExists(int id)
        {
            return _context.TeamMembers.Any(e => e.Id == id);
        }
    }
}
